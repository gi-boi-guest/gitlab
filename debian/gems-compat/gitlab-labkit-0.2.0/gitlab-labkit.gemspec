# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = "gitlab-labkit"
  spec.version = "0.2.0"
  spec.authors = ["Andrew Newdigate"]
  spec.email = ["andrew@gitlab.com"]

  spec.summary = "Instrumentation for GitLab"
  spec.homepage = "http://about.gitlab.com"
  spec.license = "MIT"

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec|tools)/}) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version = ">= 2.4.0"

  spec.add_runtime_dependency "actionpack", "~> 5"
  spec.add_runtime_dependency "activesupport", "~> 5"
  spec.add_runtime_dependency "grpc", "~> 1.15"
  spec.add_runtime_dependency "jaeger-client", "~> 0.10"
  spec.add_runtime_dependency "opentracing", "~> 0.4"

  spec.add_development_dependency "rack", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.3"
  spec.add_development_dependency "rspec", "~> 3.6.0"
  spec.add_development_dependency "rspec-parameterized", "~> 0.4"
  spec.add_development_dependency "rubocop", "~> 0.65.0"
  spec.add_development_dependency "rubocop-rspec", "~> 1.22.1"
  spec.add_development_dependency "rufo", "~> 0.6"
end
